DROP TABLE IF EXISTS `test_tbl`;
 
CREATE TABLE `test_tbl` (
      `testId` varbinary(36) NOT NULL,
      `fk_createdBy` varbinary(36) NOT NULL,
      `fk_updatedBy` varbinary(36) DEFAULT NULL,
      `createdDate` datetime NOT NULL,
      `updatedDate` datetime DEFAULT NULL,
      `active` bit(1) DEFAULT NULL,
      `deleted` bit(1) DEFAULT NULL,
      `customReferenceNo` varchar(100) DEFAULT NULL,
      PRIMARY KEY (`testId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
