package org.green;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.DriverManager;
import javax.sql.DataSource;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MybatisSpringApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(MybatisSpringApplication.class, args);
    }

    @Autowired
    private DataSource datasource;

    @Override
    public void run(String... args) throws Exception {
        String script = "C:\\Users\\greenhorn\\Desktop\\New folder\\mybatis-spring\\src\\main\\resources\\schema.sql";

        // Approch 1: using native way to create instance of Connection
//        ScriptRunner scriptRunner = new ScriptRunner(
//                DriverManager.getConnection("jdbc:mysql://localhost:3306/mybatis_test", "greenhorn", "greenhorn"));

        // Approch 2: using spring boot injected DataSource to get the connection
        ScriptRunner scriptRunner = new ScriptRunner(datasource.getConnection());
        scriptRunner.runScript(new BufferedReader(new FileReader(script)));
    }
}
